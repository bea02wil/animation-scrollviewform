﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Upgrade : MonoBehaviour
{
    #region Attributes

    [SerializeField] GameObject leftLantern;
    [SerializeField] GameObject rightLantern;   
    [SerializeField] DestroyManager destroyManager;

    [Header("StarsParticles Effect")]
    [SerializeField] GameObject brushesStarsEffect;
    [SerializeField] float durationOfStars = 1;

    [Header("SFX Settings")]
    [SerializeField] AudioClip lanternsSound;
    [SerializeField] [Range(0, 1)] float lsSoundVolume = 0.5f;
    [SerializeField] AudioClip brushesSound;
    [SerializeField] [Range(0, 1)] float bsSoundVolume = 0.5f;

    [Header("Flash Effect Parameters")]
    [SerializeField] float firstTimerValue = 0.5f;
    [SerializeField] float secondTimerValue = 1.5f;
    [SerializeField] float thirdTimerValue = 2;
    [SerializeField] float fourthTimerValue = 2.5f;

    public Button upgradeButton;
    public Text UpgradeButtonText { get; set; }
    public bool WasUpgrade { get; set; }  
    private bool lanternsAdded;
    private bool brushesAdded;
    private float flashTimer;

    #endregion

    private void Awake()
    {
        ActivateLanterns(false);
        UpgradeButtonText = upgradeButton.transform.GetChild(0).GetComponent<Text>();
    }

    private void Update()
    {
        PlayLanternsFlashEffect();
        PlayBrushesFlashEffect();
    }
 
    public void UpgradeRoofMain()
    {
        if (!WasUpgrade)
        {
            lanternsAdded = true;
            AddLanterns(true, 2);

            UpgradeButtonText.text = "Upgrade to Lvl 3";
            PlaySFX(lanternsSound, lsSoundVolume);
           
            WasUpgrade = true;            
        }
        else
        {
            AddBrushes();
            WasUpgrade = false;
        }       
    }

    #region Lanterns

    public void AddLanterns(bool state, int destroyButtonOffTime = 0)
    {
        ActivateLanterns(state);
        StartCoroutine(WaitForLanterns(state, destroyButtonOffTime));       
    }

    private void ActivateLanterns(bool state)
    {
        AppearLanterns(state);
        ActivateBrushes(false);
    }

    private void AppearLanterns(bool state)
    {
        leftLantern.gameObject.SetActive(state);
        rightLantern.gameObject.SetActive(state);        
    }

    #endregion

    #region Brushes

    private void AddBrushes()
    {
        ActivateBrushes(true);

        PlaySFX(brushesSound, bsSoundVolume);
        brushesAdded = true;

        UpgradeButtonText.text = "No more upgrades";
        upgradeButton.interactable = false;

        StartCoroutine(WaitForBsAdded());

        PlayStarsEffect();
    }

    private void PlayStarsEffect()
    {
        GameObject leftBrushStars = Instantiate(brushesStarsEffect,
               leftLantern.transform.GetChild(0).gameObject.transform.position,
               Quaternion.identity) as GameObject;

        GameObject rightBrushStars = Instantiate(brushesStarsEffect,
            rightLantern.transform.GetChild(0).gameObject.transform.position,
            Quaternion.identity) as GameObject;

        Destroy(leftBrushStars, durationOfStars);
        Destroy(rightBrushStars, durationOfStars);
    }

    private void ActivateBrushes(bool state)
    {
        AppearBrushes(state);
    } 

    private void AppearBrushes(bool state)
    {     
        leftLantern.transform.GetChild(0).gameObject.SetActive(state);
        rightLantern.transform.GetChild(0).gameObject.SetActive(state);     
    }

    #endregion

    #region FlashEffectAndButtonDisabling 

    private void PlayLanternsFlashEffect()
    {
        if (lanternsAdded)
        {
            FlashLanternsEffect();
        }
    }

    private void PlayBrushesFlashEffect()
    {
        if (brushesAdded)
        {
            StartFlashBrushes();
        }
    }

    private void FlashLanternsEffect()
    {        
        StartFlashLanterns();
        DisableButtonWhileLsSound();
    }

    private void DisableButtonWhileLsSound()
    {
        if (flashTimer != 0)
        {
            upgradeButton.interactable = false;            
        }      
    }

    private void StartFlashLanterns()
    {
        flashTimer += Time.deltaTime;

        if (flashTimer > firstTimerValue)       
            AppearLanterns(false);   
        
        if (flashTimer >= secondTimerValue)       
            AppearLanterns(true);
        
        if (flashTimer >= thirdTimerValue)        
            AppearLanterns(false);  
        
        if (flashTimer >= fourthTimerValue)
        {          
            AppearLanterns(true);           
            flashTimer = 0;
            
            lanternsAdded = false;           
        }
    }    

    private void StartFlashBrushes()
    {
        flashTimer += Time.deltaTime;
        if (flashTimer > firstTimerValue)       
            AppearBrushes(false);
        
        if (flashTimer >= secondTimerValue)      
            AppearBrushes(true);
        
        if (flashTimer >= thirdTimerValue)        
            AppearBrushes(false);
        
        if (flashTimer >= fourthTimerValue)
        {
            AppearBrushes(true);
            flashTimer = 0;
            brushesAdded = false;
        }
    }

    private IEnumerator WaitForBsAdded() //Заблочить дестрой, пока не применится эффект кистей
    {
        destroyManager.destroyButton.interactable = false;
        int brushesAddTime = 1;
        yield return new WaitForSeconds(brushesAddTime);
        destroyManager.destroyButton.interactable = true;
    } 

    #endregion

    private void PlaySFX(AudioClip audioClip, float volume)
    {
        AudioSource.PlayClipAtPoint(audioClip,
                Camera.main.transform.position, lsSoundVolume);
    }

    //Залочить кнопку, пока не применится эффект появления фонарей.
    private IEnumerator WaitForLanterns(bool state, int destroyButtonOffTime)
    {        
        yield return new WaitForSeconds(destroyButtonOffTime);
        destroyManager.destroyButton.interactable = state;
        upgradeButton.interactable = state;
    }
}
