﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanternAnimationController : MonoBehaviour
{
    #region Attributes    

    private Animator animator;

    private const string WeakWind = "Weak";
    private const string StrongWind = "Strong";
    public bool WindIsWeak { get; set; } = true;

    #endregion

    #region Core

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void AnimateWind()
    {
        if (WindIsWeak)
        {
            SwitchToStrong();
        }
        else
        {
            SwitchToWeak();
        }
    }

    #endregion

    #region SwitchMethods

    private void SwitchToWeak()
    {
        animator.SetBool(StrongWind, false);
        animator.SetBool(WeakWind, true);
        WindIsWeak = true;
    }

    private void SwitchToStrong()
    {
        if (this.isActiveAndEnabled)
        {
            animator.SetBool(WeakWind, false);
            animator.SetBool(StrongWind, true);
            WindIsWeak = false;
        }      
    }

    #endregion   
}
