﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyManager : MonoBehaviour
{
    [SerializeField] Upgrade upgradeManager;
    [SerializeField] AudioClip destroySound;
    [SerializeField] RepairManager repairManager;
    [SerializeField] [Range(0, 1)] float destroySoundVolume = 0.5f;

    public Button destroyButton;
    GameObject[] lanAnimContrl;

    private void Awake()
    {
        destroyButton.interactable = false;
        gameObject.SetActive(false);
        lanAnimContrl = GameObject.FindGameObjectsWithTag("Lantern");
    }      

    public void DestroyRoofUpgrades()
    {
        DeleteLevels();
        PlayDestroyEffect();
        repairManager.repairButton.interactable = true;
        ResetWind();
    }  

    private void PlayDestroyEffect()
    {
        AudioSource.PlayClipAtPoint(destroySound, Camera.main.transform.position, destroySoundVolume);
        gameObject.SetActive(true);
    }

    private void DeleteLevels()
    {
        upgradeManager.AddLanterns(false);
        upgradeManager.UpgradeButtonText.text = "Locked";
    }

    private void ResetWind() //вернуть сильный ветер, если сначала добавили фонари и тут же уничтожили
    {
        foreach (GameObject go in lanAnimContrl)
        {
            go.GetComponent<Animator>().
                GetComponent<LanternAnimationController>().WindIsWeak = true;
        }
    }
}
