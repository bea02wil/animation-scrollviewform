﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RepairManager : MonoBehaviour
{
    [SerializeField] DestroyManager destroyManager;
    [SerializeField] Upgrade upgradeManager;

    [Header("Roof Sprite Settings")] 
    [SerializeField] SpriteRenderer roofSprite;
    [SerializeField] Sprite roofRepairEffect;
    
    [Header("Audio Settings")]
    [SerializeField] AudioClip repairSound;
    [SerializeField] [Range(0, 1)] float repairSoundVolume = 0.5f;

    public Button repairButton;
    private Sprite roofStartSprite;

    private void Awake()
    {
        repairButton.interactable = false;
        roofStartSprite = roofSprite.sprite;
    }

    public void RepairRoof()
    {
        UpLevels();
        StateOfButtons();
        ResetUpgradeButton();
    }

    private void ResetUpgradeButton()
    {
        upgradeManager.WasUpgrade = false;
        upgradeManager.UpgradeButtonText.text = "Upgrade to Lvl 2";
    }

    private void StateOfButtons()
    {
        repairButton.interactable = false;
        upgradeManager.upgradeButton.interactable = true;
    }

    private void UpLevels()
    {
        AudioSource.PlayClipAtPoint(repairSound, Camera.main.transform.position, repairSoundVolume);
        destroyManager.gameObject.SetActive(false);
        roofSprite.sprite = roofRepairEffect;
        StartCoroutine(TimeOfRepairEffect());
    }

    private IEnumerator TimeOfRepairEffect()
    {
        float time = 0.3f;
        yield return new WaitForSeconds(time);
        roofSprite.sprite = roofStartSprite;
    }
}
