﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragManager : MonoBehaviour
{  
    private float startItemPositionX;
    private float startDeleteBtnPosX;    
    private int maxLeftSwipeOffset = 50;
    private int deleteBtnOffset = 274;

    private void Start()
    {
        SetUpSwipes();
    }

    private void SetUpSwipes()
    {
        float leftSwipePosition = transform.position.x - maxLeftSwipeOffset;
        startItemPositionX = transform.position.x;
        startDeleteBtnPosX = startItemPositionX + deleteBtnOffset;

        ItemSpawner.ListItemPosCtrl.Add(leftSwipePosition);
        ItemSpawner.ListItemPosCtrl.Add(startItemPositionX);
        ItemSpawner.ListItemPosCtrl.Add(startDeleteBtnPosX);
    }

    private void Update()
    {
        LeftSwipeControl();
        RightSwipeControl();
        DeleteButtonPosControl();      
    }

    private void DeleteButtonPosControl()
    {
        float defaultDeleteBtnPosX = ItemSpawner.ListItemPosCtrl[2];
        gameObject.transform.GetChild(0).transform.position
            = new Vector2(defaultDeleteBtnPosX, transform.position.y);
    }

    private void RightSwipeControl()
    {
        float defaultItemPositionX = ItemSpawner.ListItemPosCtrl[1];
        if (transform.position.x > defaultItemPositionX)
        {
            transform.position = new Vector2(defaultItemPositionX, transform.position.y);
        }
    }

    private void LeftSwipeControl()
    {
        float maxLeftSwipeValue = ItemSpawner.ListItemPosCtrl[0];
        if (transform.position.x < maxLeftSwipeValue)
        {
            transform.position = new Vector2(maxLeftSwipeValue, transform.position.y);           
        }
    }
}
