﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeleteButtonManager : MonoBehaviour
{
    private Button DeleteItemBtn;

    private void Start()
    {
        DeleteItemBtn = GetComponent<Button>();

        if (DeleteItemBtn != null)
            DeleteItemBtn.onClick.AddListener(DeleteItem);
    }

    private void DeleteItem()
    {
        Destroy(transform.parent.gameObject);       
    }
}
