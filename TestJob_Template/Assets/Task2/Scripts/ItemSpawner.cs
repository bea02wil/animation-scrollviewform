﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField] GameObject itemPrefab;
    [SerializeField] Transform contentPanel;
    [SerializeField] RectTransform viewportPanel;
    public static List<float> ListItemPosCtrl = new List<float>();

    public void AddItem()
    {
        GameObject item = Instantiate(itemPrefab, Vector2.zero, Quaternion.identity);

        item.GetComponent<DragItem>().viewport = viewportPanel;
        item.transform.SetParent(contentPanel);
    }
}
