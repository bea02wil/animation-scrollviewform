﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

//Код взял с открытого источника по ссылке https://forum.unity.com/threads/nested-scrollrect.268551/
// у пользователя CaptainSchnittchen и немного дописал его.
// Код позволяет детектить скролл по нужной оси.
public class DragItem : ScrollRect
{   
    private bool routeToParent = false;

    //Действие для всех родителей
    private void DoForParents<T>(Action<T> action) where T : IEventSystemHandler
    {
        Transform parent = transform.parent;
        while (parent != null)
        {
            foreach (var component in parent.GetComponents<Component>())
            {
                if (component is T)
                    action((T)(IEventSystemHandler)component);
            }
            parent = parent.parent;
        }
    }

    //Для всегда активного состояния перемещения родителей.
    public override void OnInitializePotentialDrag(PointerEventData eventData)
    {
        DoForParents<IInitializePotentialDragHandler>((parent) => { parent.OnInitializePotentialDrag(eventData); });
        base.OnInitializePotentialDrag(eventData);
    }

    //Далее события перемещения
    public override void OnDrag(UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (routeToParent)
            DoForParents<IDragHandler>((parent) => { parent.OnDrag(eventData); });
        else
        {           
            base.OnDrag(eventData);
        }         
    }
       
    public override void OnBeginDrag(UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (!horizontal && Math.Abs(eventData.delta.x) > Math.Abs(eventData.delta.y))
            routeToParent = true;
        else if (!vertical && Math.Abs(eventData.delta.x) < Math.Abs(eventData.delta.y))
            routeToParent = true;
        else
            routeToParent = false;

        if (routeToParent)
            DoForParents<IBeginDragHandler>((parent) => { parent.OnBeginDrag(eventData); });
        else
            base.OnBeginDrag(eventData); 
    }
   
    public override void OnEndDrag(UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (routeToParent)
            DoForParents<IEndDragHandler>((parent) => { parent.OnEndDrag(eventData); });
        else
        {
            //Если левый свайп не будет равен maxLeftSwipeValue, то айтем вернется на место           
            AutoRightSwipe(eventData);
        }

        routeToParent = false; 
    }

    private void AutoRightSwipe(PointerEventData eventData)
    {
        float maxLeftSwipeValue = ItemSpawner.ListItemPosCtrl[0];
        if (transform.position.x > maxLeftSwipeValue)
        {
            base.OnEndDrag(eventData);
        }
    }  
}
