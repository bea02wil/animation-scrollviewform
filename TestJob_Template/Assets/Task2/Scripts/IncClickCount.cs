﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IncClickCount : MonoBehaviour
{   
    private Button ItemTextButton;
    private Text itemText;
    private int clickCount;

    private void Start()
    {
        ItemTextButton = GetComponent<Button>();
        itemText = GetComponent<Text>();

        if (ItemTextButton != null)
            ItemTextButton.onClick.AddListener(IncreaseClickCount);
    }

    private void IncreaseClickCount()
    {
        clickCount++;
        if (clickCount == 1)
        {
            itemText.text = $"- Item - {{{clickCount}}} click";
        }
        else
        {
            itemText.text = $"- Item - {{{clickCount}}} clicks";
        }        
    }
}
